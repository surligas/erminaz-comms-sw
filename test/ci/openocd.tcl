spawn gdb -ex "set style enabled off" -ex "targ ext :3333" -ex "b fsm_task" -ex "b antenna_deploy_test" -ex "c" build/qubik-comms-sw_pcb-0.9.5_QUBIK-1.elf
set timeout 45
set error 1
set state 0
expect {
	"Breakpoint 1," {
		set state 1
		exp_continue
	}
	"Breakpoint 2," {
		set error 0
		puts "pass"
		exp_continue
	}
	"(gdb)" {
		if { $state == 1 } {
			set state 2
			send "set hqubik.settings.holdoff_timer = 1790\n"
		} elseif { $state == 2 } {
			set state 3
			send "c\n"
		} elseif { $state == 3 } {
			set state 4
			send "monitor shutdown\n"
		} else {
			send "quit\n"
		}
		exp_continue
	}
	"Quit anyway? (y or n)" {
		send "y\n"
	}
	timeout {
		set state 3
		send "\003"
		puts "timeout"
		exp_continue
	}
	eof {
		exit $error
	}
}

exit $error
