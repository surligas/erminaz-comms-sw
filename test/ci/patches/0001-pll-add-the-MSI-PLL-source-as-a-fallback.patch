From 2c43ed1133eba9f658705d935ad480e47e415470 Mon Sep 17 00:00:00 2001
From: Guennadi Liakhovetski <g.liakhovetski@gmx.de>
Date: Sun, 9 Jan 2022 13:36:56 +0100
Subject: [PATCH] pll: add the MSI PLL source as a fallback

This is primarily needed for the CI EM testing, which is missing the
TCXO, but in fact it might also be useful for mission situations, in
case the TCXO should for whatever reason fail, the system should be
able to fall back to MSI.

Signed-off-by: Guennadi Liakhovetski <g.liakhovetski@gmx.de>
---
 Core/Src/main.c | 77 ++++++++++++++++++++++++++++++++++---------------
 1 file changed, 54 insertions(+), 23 deletions(-)

diff --git a/Core/Src/main.c b/Core/Src/main.c
index 3e2d458..485e1ff 100644
--- a/Core/Src/main.c
+++ b/Core/Src/main.c
@@ -153,7 +153,7 @@ uint8_t sha_res[24];
 /* USER CODE END PV */
 
 /* Private function prototypes -----------------------------------------------*/
-void
+static void
 SystemClock_Config(void);
 static void
 MX_GPIO_Init(void);
@@ -431,14 +431,36 @@ main(void)
 	/* USER CODE END 3 */
 }
 
+#define RCC_OSCILLATOR_TYPE_HSE (RCC_OSCILLATORTYPE_LSI | \
+			RCC_OSCILLATORTYPE_HSE | RCC_OSCILLATORTYPE_LSE)
+
+#define RCC_OSCILLATOR_TYPE_MSI (RCC_OSCILLATORTYPE_LSI | \
+			RCC_OSCILLATORTYPE_MSI | RCC_OSCILLATORTYPE_LSE)
 /**
   * @brief System Clock Configuration
   * @retval None
   */
-void
+static void
 SystemClock_Config(void)
 {
-	RCC_OscInitTypeDef RCC_OscInitStruct = {0};
+	/** Initializes the RCC Oscillators according to the specified parameters
+	* in the RCC_OscInitTypeDef structure.
+	*/
+	RCC_OscInitTypeDef RCC_OscInitStruct = {
+		.OscillatorType = RCC_OSCILLATOR_TYPE_HSE,
+		.HSEState = RCC_HSE_BYPASS,
+		.LSEState = RCC_LSE_ON,
+		.LSIState = RCC_LSI_ON,
+		.PLL = {
+			.PLLState = RCC_PLL_ON,
+			.PLLSource = RCC_PLLSOURCE_HSE,
+			.PLLM = 4,
+			.PLLN = 8,
+			.PLLP = RCC_PLLP_DIV7,
+			.PLLQ = RCC_PLLQ_DIV2,
+			.PLLR = RCC_PLLR_DIV4,
+		},
+	};
 	RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
 	RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};
 
@@ -446,24 +468,27 @@ SystemClock_Config(void)
 	*/
 	HAL_PWR_EnableBkUpAccess();
 	__HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
-	/** Initializes the RCC Oscillators according to the specified parameters
-	* in the RCC_OscInitTypeDef structure.
-	*/
-	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI |
-	                                   RCC_OSCILLATORTYPE_HSE
-	                                   | RCC_OSCILLATORTYPE_LSE;
-	RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
-	RCC_OscInitStruct.LSEState = RCC_LSE_ON;
-	RCC_OscInitStruct.LSIState = RCC_LSI_ON;
-	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
-	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
-	RCC_OscInitStruct.PLL.PLLM = 4;
-	RCC_OscInitStruct.PLL.PLLN = 8;
-	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
-	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
-	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV4;
 	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
-		Error_Handler();
+		RCC_OscInitStruct = (RCC_OscInitTypeDef) {
+			.OscillatorType = RCC_OSCILLATOR_TYPE_MSI,
+			.LSEState = RCC_LSE_ON,
+			.LSIState = RCC_LSI_ON,
+			.MSIState = RCC_MSI_ON,
+			.MSICalibrationValue = 0,
+			.MSIClockRange = RCC_MSIRANGE_6,
+			.PLL = {
+				.PLLState = RCC_PLL_ON,
+				.PLLSource = RCC_PLLSOURCE_MSI,
+				.PLLM = 1,
+				.PLLN = 16,
+				.PLLP = RCC_PLLP_DIV7,
+				.PLLQ = RCC_PLLQ_DIV2,
+				.PLLR = RCC_PLLR_DIV2,
+			},
+		};
+		if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
+			Error_Handler();
+		}
 	}
 	/** Initializes the CPU, AHB and APB buses clocks
 	*/
@@ -482,9 +507,15 @@ SystemClock_Config(void)
 	PeriphClkInit.I2c2ClockSelection = RCC_I2C2CLKSOURCE_PCLK1;
 	PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
 	PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
-	PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
-	PeriphClkInit.PLLSAI1.PLLSAI1M = 4;
-	PeriphClkInit.PLLSAI1.PLLSAI1N = 12;
+	if (RCC_OscInitStruct.OscillatorType & RCC_OSCILLATORTYPE_HSE) {
+		PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
+		PeriphClkInit.PLLSAI1.PLLSAI1M = 4;
+		PeriphClkInit.PLLSAI1.PLLSAI1N = 12;
+	} else {
+		PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_MSI;
+		PeriphClkInit.PLLSAI1.PLLSAI1M = 1;
+		PeriphClkInit.PLLSAI1.PLLSAI1N = 24;
+	}
 	PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
 	PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
 	PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
