/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEST_H_
#define TEST_H_

/******************************************************************************
 ***************************** IMPORTANT NOTE *********************************
 ******************************************************************************
 * For safety reasons, (testing RX without attenuation etc) all tests are
 * disabled by default. Enable only at your local copy of the project and
 * do not commit any change on this file that has an enabled test. By default
 * all tests should be disabled and under no circumstances the satellite should
 * transmit anything by default.
 */
#define TEST_OSDLP      0
#define TEST_RX         0
#define TEST_CW         0
#define TEST_FSK_AX25   0
#define TEST_BSPK_AX25  0
#define TEST_BPSK_SSDV  0
#define TEST_ANT_DEPLOY 0
#define TEST_TX_QUEUE   0

int
test_task();

#endif /* TEST_H_ */
