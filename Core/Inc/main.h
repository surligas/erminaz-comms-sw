/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define PA_Pdet_Pin GPIO_PIN_0
#define PA_Pdet_GPIO_Port GPIOA
#define ANT_Deploy_Pin GPIO_PIN_1
#define ANT_Deploy_GPIO_Port GPIOA
#define ANT_Sense_Pin GPIO_PIN_2
#define ANT_Sense_GPIO_Port GPIOA
#define Batt_Volts_Pin GPIO_PIN_3
#define Batt_Volts_GPIO_Port GPIOA
#define SD_Detect_Pin GPIO_PIN_4
#define SD_Detect_GPIO_Port GPIOA
#define AX5043_IRQ_Pin GPIO_PIN_7
#define AX5043_IRQ_GPIO_Port GPIOA
#define AX5043_IRQ_EXTI_IRQn EXTI9_5_IRQn
#define CAN_STB_Pin GPIO_PIN_6
#define CAN_STB_GPIO_Port GPIOC
#define CAN_DRV_EN_Pin GPIO_PIN_8
#define CAN_DRV_EN_GPIO_Port GPIOA
#define AX5043_SEL_Pin GPIO_PIN_10
#define AX5043_SEL_GPIO_Port GPIOA
#define CAN1_RX_Pin GPIO_PIN_8
#define CAN1_RX_GPIO_Port GPIOB
#define CAN1_TX_Pin GPIO_PIN_9
#define CAN1_TX_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
