/*
 *
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 *  @file telemetry.h
 *
 *  @date Jul 21, 2020
 *  @brief Telemetry frame definitions
 */

#ifndef INC_TELEMETRY_H_
#define INC_TELEMETRY_H_

#include "qubik.h"
#include <stdint.h>

/**
 * Different types of telemetry frames
 */
typedef enum {
	TELEMETRY_BASIC,     //!< Standard telemetry
	TELEMETRY_NOP,       //!< Used to disable the telemetry transmission if needed
	TELEMETRY_SKIP,      //!< Continue with thye next entry on the telemetry map
	TELEMETRY_MANIFESTO, //!< Spreading to the universe the Libre Space Manifesto
	TELEMETRY_SSDV,      //!< Transmit SSDV images
	TELEMETRY_IMU        //!< Transmit IMU data
} telemetry_t;

struct telemetry_map_entry {
	telemetry_t type;
	struct tx_frame_metadata meta;
};

uint8_t
telemetry_tx_power_index(int8_t tx_power);

void
telemetry_tx_callback(struct qubik *q);

/**
 * Telemetry transmit. Passes the higher-level telemetry frame to the OSDLP
 * layer for encapsulation.
 * @param pkt the payload of the telemetry frame
 * @param length the length of the payload
 * @param vcid the VCID
 * @param meta the metadata attached to this frame
 * @return 0 zero for success, negative otherwise
 */
int
transmit_tm(const uint8_t *pkt, uint16_t length, uint8_t vcid,
            const struct tx_frame_metadata *meta);

/**
 * Retrieves the metadata for tm transmission
 *
 * @param q the qubik struct
 * @param meta the buffer where the metadata will be stored
 */
int
retrieve_meta(struct qubik *q, struct tx_frame_metadata *meta);

size_t
telemetry_basic_frame(const struct qubik *q, uint8_t *buffer);
size_t
telemetry_imu_frame(const struct qubik *q, uint8_t *buffer);


#endif /* INC_TELEMETRY_H_ */
