/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RADIO_H_
#define RADIO_H_

#include <Drivers/AX5043/include/ax5043.h>
#include <stdlib.h>
#include <stdint.h>

/**
 * The maximum RX frame size
 */
#define MAX_RX_FRAME_LEN 512

/**
 * The maximum number of RX frames that can wait at the RX queue to be processed
 * by the OSDLP logic
 */
#define MAX_RX_FRAMES    4

/**
 * The maximum TX frame size
 */
#define MAX_TX_FRAME_LEN 512

/**
 * The maximum number of outstanding frames that wait to be transmitted through
 * the AX5043
 */
#define MAX_TX_FRAMES    16

/**
 * We have identified a weird distortion originating from the PA at the end of
 * each frame. To avoid using delay inside the ISR, we use a slightly larger frame,
 * containing random bytes at the end. This ensures that the portion of the valid
 * frame is transmitted properly
 */
#define POSTAMBLE_BYTES  8

/**
 * SSDV definitions
 */
#define SSDV_PKT_SIZE         (0x100)

typedef enum {
	RADIO_STATE_INIT,      //!< Radio is initializing
	RADIO_STATE_POWER_DOWN,//!< powerdown
	RADIO_STATE_RX,        //!< RX
	RADIO_STATE_TX         //!< TX
} radio_state_t;


/**
 * Supported modulation schemes
 */
typedef enum {
	RADIO_MOD_FSK,     //!< FSK
	RADIO_MOD_BPSK,    //!< BPSK
	RADIO_MOD_BPSK_RES,//!< BPSK with residual carrier (precoded QPSK)
	RADIO_MOD_QPSK,    //!< QPSK
	RADIO_MOD_RILDOS   //!< RILDOS special modulation
} radio_modulation_t;

typedef enum {
	RADIO_ENC_RAW,
	RADIO_ENC_RS,
	RADIO_ENC_CC_1_2_RS,
	RADIO_ENC_SSDV
} radio_encoding_t;

typedef enum {
	RADIO_BAUD_600,
	RADIO_BAUD_1200,
	RADIO_BAUD_2400,
	RADIO_BAUD_4800,
	RADIO_BAUD_9600,
	RADIO_BAUD_19200,
	RADIO_BAUD_38400,
	RADIO_BAUD_125000
} radio_baud_t;

struct radio {
	radio_state_t state;
	uint16_t tx_frames;
	uint16_t rx_frames;
	uint16_t rx_frames_invalid;
	uint32_t rx_corrected_bits;
	struct ax5043_conf hax5043;
};

struct rx_frame {
	uint32_t len;
	uint8_t pdu[MAX_RX_FRAME_LEN];
};

struct tx_frame_metadata {
	radio_modulation_t mod;
	radio_encoding_t enc;
	radio_baud_t baud;
	int8_t tx_power;
	uint8_t enable_pa;
};

struct tx_frame {
	struct tx_frame_metadata meta;
	uint16_t len;
	uint32_t timeout_ms;
	uint8_t pdu[MAX_TX_FRAME_LEN];
};

/**
 * Struct with information about the decoded frame
 */
struct radio_decode {
	uint8_t crc_valid;
	uint32_t corrected_bits;
	uint32_t len;
};

int
radio_init(struct radio *hradio);

void
radio_frame_received(const uint8_t *pdu, size_t len);

void
radio_update_rx_stats(struct radio *hradio, const struct radio_decode *d);

int
radio_frame_decode(struct radio_decode *res, uint8_t *pdu, size_t len);

void
radio_tx_complete();

void
radio_task();

#endif /* RADIO_H_ */
