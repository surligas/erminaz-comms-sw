/*
 *  ERMINAZ-1: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2024, AMSAT-DL <http://amsat-dl.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INC_DIGIPEATER_H_
#define INC_DIGIPEATER_H_

#include "radio.h"

#define CALL_SIZE   (6)                 //!< size of a callsign in bytes
#define CONF_SIZE   (5)                 //!< size of the configuration struct in bytes
#define TX_SIZE     (100)               //!< maximum message size
#define MHEARD_SIZE (1000)              //!< the monitor heard queue

typedef struct dptr_msg_s {
	struct tx_frame_metadata meta;
	uint8_t delay;
	uint8_t len;
	char src_callsign[CALL_SIZE];
	char dst_callsign[CALL_SIZE];
	char msg[TX_SIZE];
} dptr_msg_t;

typedef struct mheard_s {
	uint32_t time;
	uint8_t time_valid;
	char **callsign;
} mheard_t;

void
digipeater_task();

#endif /* INC_DIGIPEATER_H_ */
