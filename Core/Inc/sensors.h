#ifndef SENSORS_H
#define SENSORS_H

#include "bg51.h"
#include "sh2.h"
#include "sh2_SensorValue.h"

#define DATA_BUF_SIZE (60)  // 60s data storage on 1 Hz update rate
#define SENSORBOARD_ADDR (0x37)

typedef struct erminaz_sh2_hdr_s {
	/** Which sensor produced this event. */
	uint8_t sensorId;

	/** @brief 8-bit unsigned integer used to track reports.
	 *
	 * The sequence number increments once for each report sent.  Gaps
	 * in the sequence numbers indicate missing or dropped reports.
	 */
	uint8_t sequence;

	/* Status of a sensor
	 *   0 - Unreliable
	 *   1 - Accuracy low
	 *   2 - Accuracy medium
	 *   3 - Accuracy high
	 */
	uint8_t status; /**< @brief bits 7-5: reserved, 4-2: exponent delay, 1-0: Accuracy */

	uint64_t timestamp;  /**< [uS] */

	uint32_t delay; /**< @brief [uS] value is delay * 2^exponent (see status) */
} erminaz_sh2_hdr_t;

typedef struct erminaz_hum_s {
	erminaz_sh2_hdr_t hdr;
	sh2_Humidity_t humidity;
} erminaz_hum_t;

typedef struct erminaz_prs_s {
	erminaz_sh2_hdr_t hdr;
	sh2_Pressure_t pressure;
} erminaz_prs_t;

typedef struct erminaz_tmp_s {
	erminaz_sh2_hdr_t hdr;
	sh2_Temperature_t temperature;
} erminaz_tmp_t;

typedef struct erminaz_rot_s {
	erminaz_sh2_hdr_t hdr;
	sh2_RotationVectorWAcc_t rotationVector;
} erminaz_rot_t;

typedef struct erminaz_mag_s {
	erminaz_sh2_hdr_t hdr;
	sh2_MagneticField_t magneticField;
} erminaz_mag_t;

typedef struct erminaz_acc_s {
	erminaz_sh2_hdr_t hdr;
	sh2_Accelerometer_t accelerometer;
} erminaz_acc_t;

typedef struct erminaz_grs_s {
	erminaz_sh2_hdr_t hdr;
	sh2_GyroIntegratedRV_t gyroIntegratedRV;
} erminaz_grs_t;

typedef struct erminaz_rgy_s {
	erminaz_sh2_hdr_t hdr;
	sh2_RawGyroscope_t rawGyroscope;
} erminaz_rgy_t;

typedef struct erminaz_sensordata_s {
	uint64_t timestamp;         // timestamp [us] of the measurement (integration time might be huge!)
	bg51_radiation_t bg51;
	erminaz_hum_t hum;  // humidity [%rel]              FIXME: not for FM
	erminaz_prs_t prs;  // pressure [hPa]               FIXME: not for FM
	erminaz_tmp_t tmp;  // temperature [degC]           FIXME: check correct SI unit
	erminaz_rot_t rot;  // quaternion i, j, k, real
	erminaz_mag_t mag;  // magnetic field [µT]
	erminaz_acc_t acc;  // acceleration                 FIXME: check if needed in FM
	erminaz_grs_t grs;  // gyroscope [deg/s]            FIXME: check if needed in FM
	erminaz_rgy_t rgy;  // raw gyroscope ADC counts     FIXME: check if needed in FM
} erminaz_sensordata_t;

extern erminaz_sensordata_t sensordata;
extern erminaz_sensordata_t sensordata_buf[];

void
sensors_init(void);
void
sensors_service(void);

#endif
