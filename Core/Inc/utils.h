/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <stm32l4xx_hal.h>
#include "FreeRTOSConfig.h"
#include <FreeRTOS.h>
#include <cmsis_os.h>

extern uint32_t millisecs;

#ifndef BIT
#define BIT(x)   (1 << (x))
#endif

#define TICKS_PER_US (configCPU_CLOCK_HZ / 1000000U)

/* Cause a compile-time error if x <= 0 */
#define compile_assert_positive(x) do {						\
	if (0) {								\
		char __error_if_negative[(x) - 1] __attribute__((unused));	\
	}									\
} while (0)

/**
 * Delays the execution of the task for the specified amount of microseconds
 * @param us the microseconds to sleep
 */
static inline void
usleep(uint32_t us)
{
	const uint32_t start = DWT->CYCCNT;
	const uint32_t us_ticks = TICKS_PER_US * us;
	while (DWT->CYCCNT - start < us_ticks);
}

/**
 *
 * @return the milliseconds from the start of the program.
 * Time is handled based on TIM2 timer.
 */
static inline uint32_t
millis()
{
	return (DWT->CYCCNT / (TICKS_PER_US * 1000UL));
//	return HAL_GetTick() / (TICKS_PER_US * 1000UL);
}

static inline uint32_t
usecs()
{
	return (DWT->CYCCNT / TICKS_PER_US);
//	return HAL_GetTick() / TICKS_PER_US;
}

/**
 * Apply TMR on a 32-bit value
 * @param a the first copy of the variable
 * @param b the second copy of the variable
 * @param c the third copy of the variable
 * @return the corrected value
 */
static inline uint32_t
tmr(uint32_t a, uint32_t b, uint32_t c)
{
	return (a & b) | (a & c) | (b & c);
}

#endif /* UTILS_H_ */
