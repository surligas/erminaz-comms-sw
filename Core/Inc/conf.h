/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef CONF_H_
#define CONF_H_

/**
 * @file conf.h
 * General configuration file
 */
#define PCB_VERSION(a,b,c) (((a) << 16) + ((b) << 8) + (c))

/*
 *  Define here your PCB version according to the tag of
 * https://gitlab.com/librespacefoundation/pq9ish/pq9ish-comms-vu-hw
 */
//#define PQ_PCB_VERSION                  PCB_VERSION(0,9,6)

#ifndef PQ_PCB_VERSION
#error "The PCB version is undefined. Please use the PCB_VERSION(a,b,c) to define it. Check your version at https://gitlab.com/librespacefoundation/pq9ish/pq9ish-comms-vu-hw"
#endif

/**
 * Enable/disable the tests performed from the default task.
 * @note set it always to 0 for the FM firmware
 */
#define QUBIK_ENABLE_TEST               0

/**
 * Serial number of the Qubik satellite
 */
#if !defined QUBIK_SN || (QUBIK_SN!=0 && QUBIK_SN!=1 && QUBIK_SN!=2 && QUBIK_SN!=3 && QUBIK_SN!=4 && QUBIK_SN!=5)
#error "The QUBIK serial number (QUBIK_SN) is undefined (valid values: 0 = QUBIK-1 / 1 = QUBIK-2)"
#endif

#if QUBIK_SN==0
#define QUBIK_NAME                      "ERMINAZ-1V"
#define QUBIK_TX_FREQ_HZ                145965000
#define QUBIK_RX_FREQ_HZ                145965000
#define OSDLP_SCID                      0x0025  // ERMINAZ-1V  --> https://sanaregistry.org/r/spacecraft/records/1803
#endif

#if QUBIK_SN==1
#define QUBIK_NAME                      "ERMINAZ-1U"
#define QUBIK_TX_FREQ_HZ                435775000
#define QUBIK_RX_FREQ_HZ                435775000
#define OSDLP_SCID                      0x0016  // ERMINAZ-1U --> https://sanaregistry.org/r/spacecraft/records/1802
#endif

#define QUBIK_SYNC_WORD                 0x3C674952
#define QUBIK_DEFAULT_TX_POWER          -2
#define QUBIK_XTAL_FREQ_HZ              26e6
#ifndef QUBIK_FLASH_MAGIC_VAL
#define QUBIK_FLASH_MAGIC_VAL           0x616c304f
#endif
#define QUBIK_STORAGE_FLASH_ADDR        0x80fe000
/** Hold of period in seconds */
#define QUBIK_POST_DEPLOY_HOLDOFF_TIME	(1 * 60)  // FIXME:
/** Telemetry interval in ms */
#define QUBIK_TELEMETRY_INTERVAL		7500      // FIXME:
/** Low power Telemetry interval in ms  */
#define QUBIK_TELEMETRY_INTERVAL_LP		15000     // FIXME:
/** Antenna deploy retry limit */
#define QUBIK_ANTENNA_RETRY_LIMIT		3
/**
 * Set it to 1, to bypass the PA using the TXDIFF signal path.
 * This maybe useful during testing
 */
#define QUBIK_BYPASS_PA                 0

/**
 * The number of entries in the telemetry map
 */
#define QUBIK_TELEMETRY_MAP_LEN         12


/**
 * The default delay for the RX-TX turnaround in milliseconds
 */
#define QUBIK_DEFAULT_TRX_TURNAROUND_MS 40

/**
 * The minimum allowed RX-TX turnaround delay in milliseconds. Used to drop
 * invalid values from TC&C
 */
#define QUBIK_MIN_TRX_TURNAROUND_MS     5

/**
 * The maximum allowed RX-TX turnaround delay in milliseconds. Used to drop
 * invalid values from TC&C
 *
 * MAKE SURE THAT THIS IS LESS THAN THE WATCHDOG PERIOD
 */
#define QUBIK_MAX_TRX_TURNAROUND_MS     2000

/**
 * The amount of seconds we allow the spacecraft not to receive
 * command from the ground without resetting
 */
#define QUBIK_GS_WDG_PERIOD_SECS        (30 * 60 * 60)

/**
 * Ramp up/Ramp down period of the power amplifier in microseconds
 */
#define PWRAMP_RAMP_PERIOD_US           200

#define AX5043_RF_SWITCH_ENABLE         ANTSEL_OUTPUT_1
#define AX5043_RF_SWITCH_DISABLE        ANTSEL_OUTPUT_0

/******************************************************************************
 ****************************** Task delays ***********************************
 *****************************************************************************/

#define WDG_TASK_DELAY_MS               400

/******************************************************************************
 ****************************** OSDLP Params **********************************
 *****************************************************************************/

#define OSDLP_TC_ITEMS_PER_QUEUE        20
#define OSDLP_TM_ITEMS_PER_QUEUE        20
#define OSDLP_MAX_TC_FRAME_SIZE         32
#define OSDLP_MAX_TC_PAYLOAD_SIZE       24  //FIXME: check! original value: 24
#define OSDLP_TM_FRAME_SIZE             128
#define OSDLP_TC_VCS                    5
#define OSDLP_TM_VCS                    5
#define OSDLP_MAX_TC_PACKET_LENGTH      (200 + 2)
#define OSDLP_MAX_TM_PACKET_LENGTH      (400 + 2)

/******************************************************************************
 ****************************** VC IDs **********************************
 *****************************************************************************/

#define VCID_MANAGEMENT                 0
#define VCID_REG_TM                     1
#define VCID_REQ_TM                     2
#define VCID_DIGIPEATER                 3
//#define VCID_EXPERIMENT                 3
//#define VCID_REG_IMU                    4
//#define VCID_REQ_IMU                    5

#endif /* CONF_H_ */
