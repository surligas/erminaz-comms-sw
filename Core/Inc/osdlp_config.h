/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEST_OSDLP_CONFIG_H_
#define TEST_OSDLP_CONFIG_H_

#include <Drivers/OSDLP/include/osdlp.h>
#include "conf.h"

/******************************************************************************
 ****************************** COP Params **********************************
 *****************************************************************************/

tc_seg_hdr_t tc_segmentation_per_vcid[OSDLP_TC_VCS] = {TC_SEG_HDR_PRESENT, TC_SEG_HDR_PRESENT,
                                                       TC_SEG_HDR_PRESENT, TC_SEG_HDR_PRESENT, TC_SEG_HDR_PRESENT
                                                      };

tc_crc_flag_t tc_crc_on[OSDLP_TC_VCS] = {TC_CRC_PRESENT, TC_CRC_PRESENT, TC_CRC_PRESENT, TC_CRC_PRESENT, TC_CRC_PRESENT};

uint8_t tc_window_width[OSDLP_TC_VCS] = {20, 20, 20, 20, 20};

/******************************************************************************
 ****************************** TM Params **********************************
 *****************************************************************************/

tm_ocf_flag_t tm_ocf_flag[OSDLP_TM_VCS] = { TM_OCF_PRESENT,
                                            TM_OCF_NOTPRESENT,
                                            TM_OCF_NOTPRESENT,
                                            TM_OCF_PRESENT,
                                            TM_OCF_NOTPRESENT
                                          };

tm_ocf_type_t tm_ocf_type[OSDLP_TM_VCS] = { TM_OCF_TYPE_1,
                                            TM_OCF_TYPE_1,
                                            TM_OCF_TYPE_1,
                                            TM_OCF_TYPE_1,
                                            TM_OCF_TYPE_1
                                          };

tm_crc_flag_t tm_crc_on[OSDLP_TM_VCS] = {TM_CRC_PRESENT,
                                         TM_CRC_PRESENT,
                                         TM_CRC_PRESENT,
                                         TM_CRC_PRESENT,
                                         TM_CRC_PRESENT
                                        };

tm_sec_hdr_flag_t tm_sec_hdr_on[OSDLP_TM_VCS] = {TM_SEC_HDR_NOTPRESENT,
                                                 TM_SEC_HDR_NOTPRESENT,
                                                 TM_SEC_HDR_NOTPRESENT,
                                                 TM_SEC_HDR_NOTPRESENT,
                                                 TM_SEC_HDR_NOTPRESENT
                                                };

uint8_t tm_sec_hdr_len[OSDLP_TM_VCS] = {0, 0, 0, 0, 0};

tm_sync_flag_t tm_sync_flag[OSDLP_TM_VCS] = {TYPE_OS_ID,
                                             TYPE_OS_ID,
                                             TYPE_OS_ID,
                                             TYPE_OS_ID,
                                             TYPE_OS_ID
                                            };

tm_stuff_state_t tm_stuffing_state[OSDLP_TM_VCS] = {TM_STUFFING_OFF,
                                                    TM_STUFFING_OFF,
                                                    TM_STUFFING_OFF,
                                                    TM_STUFFING_OFF,
                                                    TM_STUFFING_OFF
                                                   };

#endif /* TEST_OSDLP_CONFIG_H_ */
