/*
 *  ERMINAZ-1: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2024, AMSAT-DL <http://amsat-dl.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "main.h"
#include "digipeater.h"
#include "radio.h"
#include <string.h>
#include <stdio.h>
#include <FreeRTOS.h>
#include "queue.h"
#include "watchdog.h"
#include "error.h"
#include "qubik.h"
#include "telecommand.h"
#include "telemetry.h"

static struct tx_frame tmp_frame;

extern struct qubik hqubik;
extern struct watchdog hwdg;

static uint8_t tc_digi_buffer[OSDLP_MAX_TC_FRAME_SIZE];
static mheard_t
__attribute__((section(".ram2")))
mheard[MHEARD_SIZE];  /* place to store all seen callsigns */

/* TODO: add digipeater message buffer in section .ram2 */
/* TODO: add small S&F mailbox */

static int
parse_digipeater_conf(struct tx_frame_metadata *conf, uint8_t *message,
                      const uint8_t *b)
{
	if (!conf || !b) {
		return -INVAL_PARAM;
	}

	conf->enable_pa = b[0] & 0x1;

	conf->tx_power = b[1];
	if (conf->tx_power < -10 || conf->tx_power > 15) {
		return -INVAL_PARAM;
	}

	switch (b[2]) {
		case RADIO_MOD_FSK:
		case RADIO_MOD_BPSK:
		case RADIO_MOD_BPSK_RES:
		case RADIO_MOD_QPSK:
			conf->mod = (radio_modulation_t)b[2];
			break;
		default:
			return -INVAL_PARAM;
	}
	switch (b[3]) {
		case RADIO_ENC_RAW:
		case RADIO_ENC_RS:
		case RADIO_ENC_CC_1_2_RS:
			conf->enc = (radio_encoding_t)b[3];
			break;
		default:
			return -INVAL_PARAM;
	}

	switch (b[4]) {
		case RADIO_BAUD_600:
		case RADIO_BAUD_1200:
		case RADIO_BAUD_2400:
		case RADIO_BAUD_4800:
		case RADIO_BAUD_9600:
		case RADIO_BAUD_19200:
		case RADIO_BAUD_38400:
			conf->baud = (radio_baud_t)b[4];
			break;
		default:
			return -INVAL_PARAM;
	}

	/* Do not allow the RADIO_BAUD_125000 */
	if (conf->baud == RADIO_BAUD_125000) {
		return -INVAL_PARAM;
	}

	/* FIXME: Parse the digipeater message */
	uint8_t len = b[5];  //<! message length in bytes
	//if (len > OSDLP_MAX_TC_PAYLOAD_SIZE) {
	//	return -INVAL_PARAM;
	//}

	// FIXME: call sign analyzes etc etc
	//memcpy(mheard, tc_digi_buffer + CONF_SIZE, CALL_SIZE);
	return NO_ERROR;
}

void
digipeater_task()
{
	uint16_t tc_len;
	uint8_t map;
	uint8_t wdgid;

	int ret = watchdog_register(&hwdg, &wdgid, "digipeater");

	if (ret != NO_ERROR) {
		Error_Handler();
	}

	printf("Starting digipeater task...\n");

	/* Infinite loop */
	for (;;) {
		watchdog_reset_subsystem(&hwdg, wdgid);
		osDelay(10);

		/* Check if the digipeater is enabled */
		if (!hqubik.settings.digipeater_enabled) {
			continue;
		}

		/* Filter TC messages for the digipeater only */
		ret = receive_tc(tc_digi_buffer, &tc_len, &map, VCID_DIGIPEATER);
		if (ret == NO_ERROR) {
			printf("...received digipeater payload\n");
			switch (map) {
				case 1:  /* Repeat the message */
					/* Parse the digipeater parameters */
					uint8_t message[OSDLP_MAX_TC_PAYLOAD_SIZE];
					ret = parse_digipeater_conf(&tmp_frame.meta, message, tc_digi_buffer);
					if (ret) {
						continue;
					}

					memcpy(message, tc_digi_buffer + 6, tc_len - 5);
					message[tc_len - 5] = 0;
					printf("%d:%d> repeating message '%s', len=%d\n", VCID_DIGIPEATER, map, message,
					       tc_len);
					memcpy(&tmp_frame.pdu, tc_digi_buffer, tc_len);
					transmit_tm(tmp_frame.pdu, tc_len, VCID_DIGIPEATER, &tmp_frame.meta);
					break;
				default:
					printf("VCID_DIGIPEATER: unknown map id!\n");
			}
		}
	}
}
