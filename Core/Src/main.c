/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "conf.h"
#include "test.h"
#include "watchdog.h"
#include "radio.h"
#include "max17261_driver.h"
#include "power.h"
#include "antenna.h"
#include "osdlp.h"
#include "osdlp_queue_handle.h"
#include "fsm.h"
#include "qubik.h"
#include "telemetry.h"
#include "telecommand.h"
#include "manifesto.h"
#include "digipeater.h"
#include "sha256.h"
#include "sensors.h"
#include <string.h>
#include <stdio.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

CRC_HandleTypeDef hcrc;

I2C_HandleTypeDef hi2c2;

IWDG_HandleTypeDef hiwdg;

RTC_HandleTypeDef hrtc;

SPI_HandleTypeDef hspi2;

TIM_HandleTypeDef htim2;

DMA_HandleTypeDef hdma_memtomem_dma1_channel1;
osThreadId defaultTaskHandle;
uint32_t defaultTaskBuffer[ 256 ];
osStaticThreadDef_t defaultTaskControlBlock;
osThreadId wdg_taskHandle;
uint32_t wdg_task_buffer[ 128 ];
osStaticThreadDef_t wdg_task_ctrl_block;
osThreadId osdlp_rx_taskHandle;
uint32_t osdlp_task_buffer[ 512 ];
osStaticThreadDef_t osdlp_task_ctrl_block;
osThreadId radio_taskHandle;
uint32_t radio_task_buffer[ 512 ];
osStaticThreadDef_t radio_task_ctrl_block;
osThreadId osdlp_tx_taskHandle;
uint32_t osdlp_tx_task_buffer[ 512 ];
osStaticThreadDef_t osdlp_tx_task_ctrl_block;
osThreadId fsm_taskHandle;
uint32_t fsm_task_buffer[ 512 ];
osStaticThreadDef_t fsm_task_ctrl_block;
osThreadId pwr_taskHandle;
uint32_t pwr_task_buffer[ 512 ];
osStaticThreadDef_t pwr_task_ctrl_block;
osThreadId management_taskHandle;
uint32_t mgmt_buffer[ 512 ];
osStaticThreadDef_t mgmt_ctrl_block;
osThreadId tm_request_taskHandle;
uint32_t tm_rqst_buffer[ 512 ];
osStaticThreadDef_t tm_rqst_ctrl_block;
osThreadId digipeater_taskHandle;
uint32_t digi_buffer[ 512 ];
osStaticThreadDef_t digi_ctrl_block;
osTimerId telemetry_timerHandle;
osStaticTimerDef_t telemetry_timer_crl_block;
osTimerId uptime_timerHandle;
osStaticTimerDef_t uptime_timer_ctrl_block;
osMutexId wdg_mtxHandle;
osStaticMutexDef_t wdg_mtx_ctrl_block;
osMutexId ax5043_mtxHandle;
osStaticMutexDef_t ax5043_mtx_ctrl_block;
osMutexId osdlp_rx_mtxHandle;
osStaticMutexDef_t osdlp_queue_mtx_ctrl_block;
osMutexId osdlp_tx_mtxHandle;
osStaticMutexDef_t osdlp_tx_mtx_ctrl_block;
osMutexId pwr_mtxHandle;
osStaticMutexDef_t pwr_mtx_ctrl_block;
/* USER CODE BEGIN PV */
struct watchdog hwdg;
struct qubik hqubik;
uint8_t reset_flag;
//struct wdg_hist wdg_history;
/* Pointer to a region in SRAM2 where watchdog reset mask is stored*/
uint8_t *wdg_rst_ptr = (uint8_t *)0x10007800;

struct wdg_rec wdg_recorder;
/*
 * The CSMIS interface from ST for reasons that are not obvious support only
 * messages of scalar data types. A workaround is to use a memory pool.
 * However, for unexplained reasons memory pools are not supported with
 * static memory allocation configuration. We ended using the native
 * FREERTOS API...
 */
StaticQueue_t rx_queue_priv;
uint8_t rx_queue_pool[MAX_RX_FRAMES * sizeof(struct rx_frame)];
QueueHandle_t rx_queue;

StaticQueue_t tx_queue_priv;
uint8_t tx_queue_pool[MAX_TX_FRAMES * sizeof(struct tx_frame)];
QueueHandle_t tx_queue;
struct rx_frame r;
static struct tx_frame tx_msg;
uint8_t priorities[OSDLP_TM_VCS] = {0, 1, 2, 3, 4};
bool last_pkt_avail = true;

uint8_t tm_request_buf[OSDLP_MAX_TM_PACKET_LENGTH];
uint8_t tm_attr_req_buf[sizeof(hqubik.settings.telemetry_map)];
static SHA256_CTX sha_ctx;
uint8_t sha_res[24];


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void
SystemClock_Config(void);
static void
MX_GPIO_Init(void);
static void
MX_DMA_Init(void);
static void
MX_CRC_Init(void);
static void
MX_SPI2_Init(void);
static void
MX_TIM2_Init(void);
static void
MX_ADC1_Init(void);
static void
MX_RTC_Init(void);
static void
MX_I2C2_Init(void);
static void
MX_IWDG_Init(void);
void
StartDefaultTask(void const *argument);
void
start_wdg_task(void const *argument);
void
start_osdlp_task(void const *argument);
void
start_radio_task(void const *argument);
void
start_osdlp_tx_task(void const *argument);
void
start_fsm_task(void const *argument);
void
start_pwr_task(void const *argument);
void
start_mgmt_task(void const *argument);
void
start_tm_rqst_task(void const *argument);
void
start_exp_task(void const *argument);
void
start_digi_task(void const *argument);
void
telemetry_timer_callback(void const *argument);
void
incr_uptime(void const *argument);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
typedef enum {
	RESET_CAUSE_UNKNOWN = 0,
	RESET_CAUSE_LOW_POWER_RESET,
	RESET_CAUSE_WINDOW_WATCHDOG_RESET,
	RESET_CAUSE_INDEPENDENT_WATCHDOG_RESET,
	RESET_CAUSE_SOFTWARE_RESET,
	RESET_CAUSE_POWER_ON_POWER_DOWN_RESET,
	RESET_CAUSE_EXTERNAL_RESET_PIN_RESET,
	RESET_CAUSE_BROWNOUT_RESET,
} reset_cause_t;

void
reset_cause_flags_update(void)
{
	reset_flag = 0;
	if (__HAL_RCC_GET_FLAG(RCC_FLAG_LPWRRST)) {
		reset_flag |= 1UL << RESET_CAUSE_LOW_POWER_RESET;
	}
	if (__HAL_RCC_GET_FLAG(RCC_FLAG_WWDGRST)) {
		reset_flag |= 1UL << RESET_CAUSE_WINDOW_WATCHDOG_RESET;
	}
	if (__HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST)) {
		reset_flag |= 1UL << RESET_CAUSE_INDEPENDENT_WATCHDOG_RESET;
	}
	if (__HAL_RCC_GET_FLAG(RCC_FLAG_SFTRST)) {
		reset_flag |= 1UL << RESET_CAUSE_SOFTWARE_RESET;
	}
	if (__HAL_RCC_GET_FLAG(RCC_FLAG_PINRST)) {
		reset_flag |= 1UL << RESET_CAUSE_EXTERNAL_RESET_PIN_RESET;
	}
	if (__HAL_RCC_GET_FLAG(RCC_FLAG_BORRST)) {
		reset_flag |= 1UL << RESET_CAUSE_BROWNOUT_RESET;
	}

	__HAL_RCC_CLEAR_RESET_FLAGS();
}
int ctr = 0;

int
_write(int file, char *ptr, int len)
{
	/* Implement your write code here, this is used by puts and printf for example */
	int i = 0;
	for (i = 0 ; i < len ; i++)
		ITM_SendChar((*ptr++));
	return len;
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int
main(void)
{
	/* USER CODE BEGIN 1 */
	/* Get last reason of reset */
	reset_cause_flags_update();

	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */
	/* Enable DWT for us resolution delay */
#ifndef DEBUG
	if (!(CoreDebug->DEMCR & CoreDebug_DEMCR_TRCENA_Msk)) {
		CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
		DWT->CYCCNT = 0;
		DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
	}
#else
	/* force the DWT timer to run in debug sessions: */
	CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
	DWT->CYCCNT = 0;
	DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;

#endif
	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_CRC_Init();
	MX_SPI2_Init();
	MX_TIM2_Init();
	MX_ADC1_Init();
	MX_RTC_Init();
	MX_I2C2_Init();
	MX_IWDG_Init();
	/* USER CODE BEGIN 2 */

	/* USER CODE END 2 */

	/* Create the mutex(es) */
	/* definition and creation of wdg_mtx */
	osMutexStaticDef(wdg_mtx, &wdg_mtx_ctrl_block);
	wdg_mtxHandle = osMutexCreate(osMutex(wdg_mtx));

	/* definition and creation of ax5043_mtx */
	osMutexStaticDef(ax5043_mtx, &ax5043_mtx_ctrl_block);
	ax5043_mtxHandle = osMutexCreate(osMutex(ax5043_mtx));

	/* definition and creation of osdlp_rx_mtx */
	osMutexStaticDef(osdlp_rx_mtx, &osdlp_queue_mtx_ctrl_block);
	osdlp_rx_mtxHandle = osMutexCreate(osMutex(osdlp_rx_mtx));

	/* definition and creation of osdlp_tx_mtx */
	osMutexStaticDef(osdlp_tx_mtx, &osdlp_tx_mtx_ctrl_block);
	osdlp_tx_mtxHandle = osMutexCreate(osMutex(osdlp_tx_mtx));

	/* definition and creation of pwr_mtx */
	osMutexStaticDef(pwr_mtx, &pwr_mtx_ctrl_block);
	pwr_mtxHandle = osMutexCreate(osMutex(pwr_mtx));

	/* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */
	/* USER CODE END RTOS_MUTEX */

	/* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
	/* USER CODE END RTOS_SEMAPHORES */

	/* Create the timer(s) */
	/* definition and creation of telemetry_timer */
	osTimerStaticDef(telemetry_timer, telemetry_timer_callback,
	                 &telemetry_timer_crl_block);
	telemetry_timerHandle = osTimerCreate(osTimer(telemetry_timer), osTimerPeriodic,
	                                      NULL);

	/* definition and creation of uptime_timer */
	osTimerStaticDef(uptime_timer, incr_uptime, &uptime_timer_ctrl_block);
	uptime_timerHandle = osTimerCreate(osTimer(uptime_timer), osTimerPeriodic,
	                                   NULL);

	/* USER CODE BEGIN RTOS_TIMERS */
	/* Uptime ins seconds */
	osTimerStart(uptime_timerHandle, 1000);
	/* USER CODE END RTOS_TIMERS */

	/* USER CODE BEGIN RTOS_QUEUES */
	rx_queue = xQueueCreateStatic(MAX_RX_FRAMES,
	                              sizeof(struct rx_frame),
	                              rx_queue_pool,
	                              &rx_queue_priv);
	configASSERT(rx_queue);

	tx_queue = xQueueCreateStatic(MAX_TX_FRAMES,
	                              sizeof(struct tx_frame),
	                              tx_queue_pool,
	                              &tx_queue_priv);
	configASSERT(tx_queue);
	/* USER CODE END RTOS_QUEUES */

	/* Create the thread(s) */
	/* definition and creation of defaultTask */
	osThreadStaticDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 256,
	                  defaultTaskBuffer, &defaultTaskControlBlock);
	defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

	/* definition and creation of osdlp_rx_task */
	osThreadStaticDef(osdlp_rx_task, start_osdlp_task, osPriorityNormal, 0, 512,
	                  osdlp_task_buffer, &osdlp_task_ctrl_block);
	osdlp_rx_taskHandle = osThreadCreate(osThread(osdlp_rx_task), NULL);

	/* definition and creation of radio_task */
	osThreadStaticDef(radio_task, start_radio_task, osPriorityHigh, 0, 512,
	                  radio_task_buffer, &radio_task_ctrl_block);
	radio_taskHandle = osThreadCreate(osThread(radio_task), NULL);

	/* definition and creation of osdlp_tx_task */
	osThreadStaticDef(osdlp_tx_task, start_osdlp_tx_task, osPriorityNormal, 0, 512,
	                  osdlp_tx_task_buffer, &osdlp_tx_task_ctrl_block);
	osdlp_tx_taskHandle = osThreadCreate(osThread(osdlp_tx_task), NULL);

	/* definition and creation of fsm_task */
	osThreadStaticDef(fsm_task, start_fsm_task, osPriorityNormal, 0, 512,
	                  fsm_task_buffer, &fsm_task_ctrl_block);
	fsm_taskHandle = osThreadCreate(osThread(fsm_task), NULL);

	/* definition and creation of pwr_task */
	osThreadStaticDef(pwr_task, start_pwr_task, osPriorityBelowNormal, 0, 512,
	                  pwr_task_buffer, &pwr_task_ctrl_block);
	pwr_taskHandle = osThreadCreate(osThread(pwr_task), NULL);

	/* definition and creation of management_task */
	osThreadStaticDef(management_task, start_mgmt_task, osPriorityHigh, 0, 512,
	                  mgmt_buffer, &mgmt_ctrl_block);
	management_taskHandle = osThreadCreate(osThread(management_task), NULL);

	/* definition and creation of tm_request_task */
	osThreadStaticDef(tm_request_task, start_tm_rqst_task, osPriorityNormal, 0, 512,
	                  tm_rqst_buffer, &tm_rqst_ctrl_block);
	tm_request_taskHandle = osThreadCreate(osThread(tm_request_task), NULL);

	/* definition and creation of digipeater_task */
	osThreadStaticDef(digipeater_task, start_digi_task, osPriorityNormal, 0, 512,
	                  digi_buffer, &digi_ctrl_block);
	digipeater_taskHandle = osThreadCreate(osThread(digipeater_task), NULL);

	/* definition and creation of wdg_task */
	osThreadStaticDef(wdg_task, start_wdg_task, osPriorityHigh, 0, 128,
	                  wdg_task_buffer, &wdg_task_ctrl_block);
	wdg_taskHandle = osThreadCreate(osThread(wdg_task), NULL);

	/* USER CODE BEGIN RTOS_THREADS */

	int ret = watchdog_init(&hwdg, &hiwdg, wdg_mtxHandle, 9, &wdg_recorder,
	                        wdg_rst_ptr);
	if (ret) {
		Error_Handler();
	}
	ret = qubik_init(&hqubik);
	if (ret) {
		Error_Handler();
	}
	/* USER CODE END RTOS_THREADS */

	/* Start scheduler */
	printf("QUBIK OS Start!\n");  /* FIXME: remove */
	osKernelStart();

	/* We should never get here as control is now taken by the scheduler */
	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1) {
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void
SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
	RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

	/** Configure LSE Drive Capability
	*/
	HAL_PWR_EnableBkUpAccess();
	__HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
	/** Initializes the RCC Oscillators according to the specified parameters
	* in the RCC_OscInitTypeDef structure.
	*/
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI |
	                                   RCC_OSCILLATORTYPE_HSE
	                                   | RCC_OSCILLATORTYPE_LSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
	RCC_OscInitStruct.LSEState = RCC_LSE_ON;
	RCC_OscInitStruct.LSIState = RCC_LSI_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 4;
	RCC_OscInitStruct.PLL.PLLN = 8;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV4;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB buses clocks
	*/
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
	                              | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK) {
		Error_Handler();
	}
	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC | RCC_PERIPHCLK_I2C2
	                                     | RCC_PERIPHCLK_ADC;
	PeriphClkInit.I2c2ClockSelection = RCC_I2C2CLKSOURCE_PCLK1;
	PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
	PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
	PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
	PeriphClkInit.PLLSAI1.PLLSAI1M = 4;
	PeriphClkInit.PLLSAI1.PLLSAI1N = 12;
	PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
	PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
	PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
	PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_ADC1CLK;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK) {
		Error_Handler();
	}
	/** Configure the main internal regulator output voltage
	*/
	if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK) {
		Error_Handler();
	}
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void
MX_ADC1_Init(void)
{

	/* USER CODE BEGIN ADC1_Init 0 */

	/* USER CODE END ADC1_Init 0 */

	ADC_MultiModeTypeDef multimode = {0};
	ADC_ChannelConfTypeDef sConfig = {0};

	/* USER CODE BEGIN ADC1_Init 1 */

	/* USER CODE END ADC1_Init 1 */
	/** Common config
	*/
	hadc1.Instance = ADC1;
	hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
	hadc1.Init.Resolution = ADC_RESOLUTION_12B;
	hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
	hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
	hadc1.Init.LowPowerAutoWait = DISABLE;
	hadc1.Init.ContinuousConvMode = DISABLE;
	hadc1.Init.NbrOfConversion = 1;
	hadc1.Init.DiscontinuousConvMode = DISABLE;
	hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
	hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
	hadc1.Init.DMAContinuousRequests = DISABLE;
	hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
	hadc1.Init.OversamplingMode = DISABLE;
	if (HAL_ADC_Init(&hadc1) != HAL_OK) {
		Error_Handler();
	}
	/** Configure the ADC multi-mode
	*/
	multimode.Mode = ADC_MODE_INDEPENDENT;
	if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK) {
		Error_Handler();
	}
	/** Configure Regular Channel
	*/
	sConfig.Channel = ADC_CHANNEL_8;
	sConfig.Rank = ADC_REGULAR_RANK_1;
	sConfig.SamplingTime = ADC_SAMPLETIME_24CYCLES_5;
	sConfig.SingleDiff = ADC_SINGLE_ENDED;
	sConfig.OffsetNumber = ADC_OFFSET_NONE;
	sConfig.Offset = 0;
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN ADC1_Init 2 */

	/* USER CODE END ADC1_Init 2 */

}

/**
  * @brief CRC Initialization Function
  * @param None
  * @retval None
  */
static void
MX_CRC_Init(void)
{

	/* USER CODE BEGIN CRC_Init 0 */

	/* USER CODE END CRC_Init 0 */

	/* USER CODE BEGIN CRC_Init 1 */

	/* USER CODE END CRC_Init 1 */
	hcrc.Instance = CRC;
	hcrc.Init.DefaultPolynomialUse = DEFAULT_POLYNOMIAL_ENABLE;
	hcrc.Init.DefaultInitValueUse = DEFAULT_INIT_VALUE_ENABLE;
	hcrc.Init.InputDataInversionMode = CRC_INPUTDATA_INVERSION_NONE;
	hcrc.Init.OutputDataInversionMode = CRC_OUTPUTDATA_INVERSION_DISABLE;
	hcrc.InputDataFormat = CRC_INPUTDATA_FORMAT_BYTES;
	if (HAL_CRC_Init(&hcrc) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN CRC_Init 2 */

	/* USER CODE END CRC_Init 2 */

}

/**
  * @brief I2C2 Initialization Function
  * @param None
  * @retval None
  */
static void
MX_I2C2_Init(void)
{

	/* USER CODE BEGIN I2C2_Init 0 */

	/* USER CODE END I2C2_Init 0 */

	/* USER CODE BEGIN I2C2_Init 1 */

	/* USER CODE END I2C2_Init 1 */
	hi2c2.Instance = I2C2;
	hi2c2.Init.Timing = 0x00303D5B;
	hi2c2.Init.OwnAddress1 = 0;
	hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	hi2c2.Init.OwnAddress2 = 0;
	hi2c2.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
	hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
	if (HAL_I2C_Init(&hi2c2) != HAL_OK) {
		Error_Handler();
	}
	/** Configure Analogue filter
	*/
	if (HAL_I2CEx_ConfigAnalogFilter(&hi2c2, I2C_ANALOGFILTER_ENABLE) != HAL_OK) {
		Error_Handler();
	}
	/** Configure Digital filter
	*/
	if (HAL_I2CEx_ConfigDigitalFilter(&hi2c2, 0) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN I2C2_Init 2 */

	/* USER CODE END I2C2_Init 2 */

}

/**
  * @brief IWDG Initialization Function
  * @param None
  * @retval None
  */
static void
MX_IWDG_Init(void)
{

	/* USER CODE BEGIN IWDG_Init 0 */

	/* USER CODE END IWDG_Init 0 */

	/* USER CODE BEGIN IWDG_Init 1 */

	/* USER CODE END IWDG_Init 1 */
	hiwdg.Instance = IWDG;
	hiwdg.Init.Prescaler = IWDG_PRESCALER_256;
	hiwdg.Init.Window = 4095;
	hiwdg.Init.Reload = 4095;
	if (HAL_IWDG_Init(&hiwdg) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN IWDG_Init 2 */

	/* USER CODE END IWDG_Init 2 */

}

/**
  * @brief RTC Initialization Function
  * @param None
  * @retval None
  */
static void
MX_RTC_Init(void)
{

	/* USER CODE BEGIN RTC_Init 0 */

	/* USER CODE END RTC_Init 0 */

	/* USER CODE BEGIN RTC_Init 1 */

	/* USER CODE END RTC_Init 1 */
	/** Initialize RTC Only
	*/
	hrtc.Instance = RTC;
	hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
	hrtc.Init.AsynchPrediv = 127;
	hrtc.Init.SynchPrediv = 255;
	hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
	hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
	hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
	hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
	if (HAL_RTC_Init(&hrtc) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN RTC_Init 2 */

	/* USER CODE END RTC_Init 2 */

}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void
MX_SPI2_Init(void)
{

	/* USER CODE BEGIN SPI2_Init 0 */

	/* USER CODE END SPI2_Init 0 */

	/* USER CODE BEGIN SPI2_Init 1 */

	/* USER CODE END SPI2_Init 1 */
	/* SPI2 parameter configuration*/
	hspi2.Instance = SPI2;
	hspi2.Init.Mode = SPI_MODE_MASTER;
	hspi2.Init.Direction = SPI_DIRECTION_2LINES;
	hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
	hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
	hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
	hspi2.Init.NSS = SPI_NSS_SOFT;
	hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
	hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
	hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
	hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	hspi2.Init.CRCPolynomial = 7;
	hspi2.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
	hspi2.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;
	if (HAL_SPI_Init(&hspi2) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN SPI2_Init 2 */

	/* USER CODE END SPI2_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void
MX_TIM2_Init(void)
{

	/* USER CODE BEGIN TIM2_Init 0 */

	/* USER CODE END TIM2_Init 0 */

	TIM_ClockConfigTypeDef sClockSourceConfig = {0};
	TIM_MasterConfigTypeDef sMasterConfig = {0};

	/* USER CODE BEGIN TIM2_Init 1 */

	/* USER CODE END TIM2_Init 1 */
	htim2.Instance = TIM2;
	htim2.Init.Prescaler = 48000;
	htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim2.Init.Period = 0xFFFFFFFF;
	htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&htim2) != HAL_OK) {
		Error_Handler();
	}
	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK) {
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN TIM2_Init 2 */

	/* USER CODE END TIM2_Init 2 */

}

/**
  * Enable DMA controller clock
  * Configure DMA for memory to memory transfers
  *   hdma_memtomem_dma1_channel1
  */
static void
MX_DMA_Init(void)
{

	/* DMA controller clock enable */
	__HAL_RCC_DMA1_CLK_ENABLE();

	/* Configure DMA request hdma_memtomem_dma1_channel1 on DMA1_Channel1 */
	hdma_memtomem_dma1_channel1.Instance = DMA1_Channel1;
	hdma_memtomem_dma1_channel1.Init.Request = DMA_REQUEST_0;
	hdma_memtomem_dma1_channel1.Init.Direction = DMA_MEMORY_TO_MEMORY;
	hdma_memtomem_dma1_channel1.Init.PeriphInc = DMA_PINC_ENABLE;
	hdma_memtomem_dma1_channel1.Init.MemInc = DMA_MINC_ENABLE;
	hdma_memtomem_dma1_channel1.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	hdma_memtomem_dma1_channel1.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
	hdma_memtomem_dma1_channel1.Init.Mode = DMA_NORMAL;
	hdma_memtomem_dma1_channel1.Init.Priority = DMA_PRIORITY_LOW;
	if (HAL_DMA_Init(&hdma_memtomem_dma1_channel1) != HAL_OK) {
		Error_Handler();
	}

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void
MX_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(ANT_Deploy_GPIO_Port, ANT_Deploy_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(CAN_STB_GPIO_Port, CAN_STB_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOA, CAN_DRV_EN_Pin | AX5043_SEL_Pin, GPIO_PIN_SET);

	/*Configure GPIO pins : PC13 PC0 PC1 PC4
	                         PC5 PC7 PC8 PC9
	                         PC10 PC11 PC12 */
	GPIO_InitStruct.Pin = GPIO_PIN_13 | GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_4
	                      | GPIO_PIN_5 | GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9
	                      | GPIO_PIN_10 | GPIO_PIN_11 | GPIO_PIN_12;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pin : PH1 */
	GPIO_InitStruct.Pin = GPIO_PIN_1;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

	/*Configure GPIO pins : ANT_Deploy_Pin CAN_DRV_EN_Pin */
	GPIO_InitStruct.Pin = ANT_Deploy_Pin | CAN_DRV_EN_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pin : ANT_Sense_Pin */
	GPIO_InitStruct.Pin = ANT_Sense_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(ANT_Sense_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : SD_Detect_Pin */
	GPIO_InitStruct.Pin = SD_Detect_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(SD_Detect_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : PA5 PA6 PA9 PA11
	                         PA12 PA15 */
	GPIO_InitStruct.Pin = GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_9 | GPIO_PIN_11
	                      | GPIO_PIN_12 | GPIO_PIN_15;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pin : AX5043_IRQ_Pin */
	GPIO_InitStruct.Pin = AX5043_IRQ_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(AX5043_IRQ_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : PB0 PB1 PB2 PB12
	                         PB14 PB15 PB4 PB5
	                         PB6 PB7 */
	GPIO_InitStruct.Pin = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_12
	                      | GPIO_PIN_14 | GPIO_PIN_15 | GPIO_PIN_4 | GPIO_PIN_5
	                      | GPIO_PIN_6 | GPIO_PIN_7;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pin : CAN_STB_Pin */
	GPIO_InitStruct.Pin = CAN_STB_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(CAN_STB_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : AX5043_SEL_Pin */
	GPIO_InitStruct.Pin = AX5043_SEL_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(AX5043_SEL_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : PD2 */
	GPIO_InitStruct.Pin = GPIO_PIN_2;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

	/*Configure GPIO pins : CAN1_RX_Pin CAN1_TX_Pin */
	GPIO_InitStruct.Pin = CAN1_RX_Pin | CAN1_TX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF9_CAN1;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/* EXTI interrupt init*/
	HAL_NVIC_SetPriority(EXTI9_5_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

}

/* USER CODE BEGIN 4 */
void
HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if (GPIO_Pin == GPIO_PIN_7) {
		ax5043_irq_callback();
	}
}
/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
 * @brief  Function implementing the defaultTask thread.
 * @param  argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartDefaultTask */
void
StartDefaultTask(void const *argument)
{
	/* USER CODE BEGIN 5 */
#if QUBIK_ENABLE_TEST
	int ret = test_task();
	if (ret) {
		Error_Handler();
	}
	//while(1);
#else
	uint8_t wdgid;

	int ret = watchdog_register(&hwdg, &wdgid, "default");

	if (ret != NO_ERROR) {
		Error_Handler();
	}

	while (1) {
		watchdog_reset_subsystem(&hwdg, wdgid);
		osDelay(1000);
	}
#endif
	/* USER CODE END 5 */
}

/* USER CODE BEGIN Header_start_wdg_task */
/**
 * @brief Function implementing the wdg_task thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_start_wdg_task */
void
start_wdg_task(void const *argument)
{
	/* USER CODE BEGIN start_wdg_task */
	/* Infinite loop */
	for (;;) {
		watchdog_reset(&hwdg);
		osDelay(WDG_TASK_DELAY_MS);
	}
	/* USER CODE END start_wdg_task */
}

/* USER CODE BEGIN Header_start_osdlp_task */
/**
 * @brief Function implementing the osdlp_task thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_start_osdlp_task */
void
start_osdlp_task(void const *argument)
{
	/* USER CODE BEGIN start_osdlp_task */
	/*Initialize osdlp*/
	initialize_osdlp();
	struct rx_frame *rx_f = &r;
	struct tc_transfer_frame *tc;
	struct tm_transfer_frame *tm;
	struct tx_frame_metadata meta;
	uint8_t wdgid;
	int ret = watchdog_register(&hwdg, &wdgid, "osdlp_rx");

	if (ret != NO_ERROR) {
		Error_Handler();
	}

	for (;;) {
		watchdog_reset_subsystem(&hwdg, wdgid);
		if (xQueueReceive(rx_queue, rx_f, pdMS_TO_TICKS(5000)) != pdPASS) {
			continue;
		}
		/* That was quite a big delay! Reset again the dog! */
		watchdog_reset_subsystem(&hwdg, wdgid);

		struct radio_decode decode_info;
		ret = radio_frame_decode(&decode_info, rx_f->pdu, rx_f->len);
		radio_update_rx_stats(&hqubik.hradio, &decode_info);
		if (ret || decode_info.crc_valid == 0) {
			printf("ERROR: CRC invalid!\n");
			continue;
		}

		/* Now that we know that the frame is probably valid, we delay
		 * the propagation of the TC frame in order to allow the
		 * GS to switch over from TX to RX
		 */
		osDelay(hqubik.settings.trx_delay_ms);

		/*
		 * Error correction is performed in place, but the length of
		 * the frame is different after the error correction and the
		 * removal of the PHY CRC
		 */
		ret = osdlp_tc_receive(rx_f->pdu, decode_info.len);
		if (ret) {
			printf("ERROR #%d: TC invalid!\n", ret);
			continue;
		}

		/*
		 * Since all checks on the received frame have passed
		 * we can reset the ground watchdog
		 */
		hwdg.last_contact_secs = 0;

		tc = get_last_tc();
		if (osMutexWait(osdlp_tx_mtxHandle, 200) == osOK) {
			printf("last tc %d:%d\n", tc->primary_hdr.vcid, tc->primary_hdr.ctrl_cmd);
			ret = tm_get_tx_config(&tm, tc->primary_hdr.vcid);
			if (ret || tc->primary_hdr.vcid == 1 ||
			    tc->primary_hdr.vcid == 6) {
				osMutexRelease(osdlp_tx_mtxHandle);
				continue;
			}
			osdlp_prepare_clcw(tc, tm->ocf);
			retrieve_meta(&hqubik, &meta);
			register_meta(&meta);
			osdlp_tm_transmit_idle_fdu(tm, tc->primary_hdr.vcid);
			osMutexRelease(osdlp_tx_mtxHandle);
		}
	}
	/* USER CODE END start_osdlp_task */
}

/* USER CODE BEGIN Header_start_radio_task */
/**
 * @brief Function implementing the radio_task thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_start_radio_task */
void
start_radio_task(void const *argument)
{
	/* USER CODE BEGIN start_radio_task */
	radio_task();
	/* USER CODE END start_radio_task */
}

/* USER CODE BEGIN Header_start_osdlp_tx_task */
/**
 * @brief Function implementing the osdlp_tx_task thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_start_osdlp_tx_task */
void
start_osdlp_tx_task(void const *argument)
{
	/* USER CODE BEGIN start_osdlp_tx_task */
	volatile uint8_t cap = 0;
	struct tx_bundle tx_bun;
	uint16_t len;
	uint8_t wdgid;
	int ret = watchdog_register(&hwdg, &wdgid, "osdlp_tx");

	if (ret != NO_ERROR) {
		Error_Handler();
	}

	/* Infinite loop */
	for (;;) {
		watchdog_reset_subsystem(&hwdg, wdgid);
		osDelay(10);
		for (size_t i = 0; i < OSDLP_TM_VCS; i++) {
			cap = 0;
			if (osMutexWait(osdlp_tx_mtxHandle, 200) == osOK) {
				cap = tx_queues[priorities[i]].inqueue;
				osMutexRelease(osdlp_tx_mtxHandle);
			} else {
				continue;
			}
			while (cap > 0) {
				if (osMutexWait(osdlp_tx_mtxHandle, 200) == osOK) {
					dequeue(&tx_queues[priorities[i]], &tx_bun, &len);
					cap = tx_queues[priorities[i]].inqueue;
					osMutexRelease(osdlp_tx_mtxHandle);
				} else {
					break;
				}
				if (i == 2) {
					osDelay(500);
				}
				tx_msg.meta = tx_bun.meta;
				memcpy(tx_msg.pdu, tx_bun.frame, OSDLP_TM_FRAME_SIZE);
				tx_msg.len = OSDLP_TM_FRAME_SIZE;
				tx_msg.timeout_ms = 20;
				xQueueSend(tx_queue, &tx_msg, pdMS_TO_TICKS(5000));
			}
		}
	}
	/* USER CODE END start_osdlp_tx_task */
}

/* USER CODE BEGIN Header_start_fsm_task */
/**
 * @brief Function implementing the fsm_task thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_start_fsm_task */
void
start_fsm_task(void const *argument)
{
	/* USER CODE BEGIN start_fsm_task */
	fsm_task();
	/* USER CODE END start_fsm_task */
}

/* USER CODE BEGIN Header_start_pwr_task */
/**
 * @brief Function implementing the pwr_task thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_start_pwr_task */
void
start_pwr_task(void const *argument)
{
	/* USER CODE BEGIN start_pwr_task */
	power_task();
	/* USER CODE END start_pwr_task */
}

/* USER CODE BEGIN Header_start_mgmt_task */
/**
 * @brief Function implementing the management_task thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_start_mgmt_task */
void
start_mgmt_task(void const *argument)
{
	/* USER CODE BEGIN start_mgmt_task */
	uint8_t tc_buffer[OSDLP_MAX_TC_PAYLOAD_SIZE];
	uint16_t tc_len = 0;
	uint8_t map = 0;
	struct tx_frame_metadata meta;
	uint8_t wdgid;
	int ret = watchdog_register(&hwdg, &wdgid, "mgmt");

	if (ret != NO_ERROR) {
		Error_Handler();
	}

	/* Infinite loop */
	for (;;) {
		watchdog_reset_subsystem(&hwdg, wdgid);
		osDelay(10);

		/* try to retrieve the incoming TC */
		ret = receive_tc(tc_buffer, &tc_len, &map, VCID_MANAGEMENT);
		if (ret == NO_ERROR) {
			switch (map) {
				case 1: 	/* Change periodic telemetry attributes */
					/*
					 * we expect a set of QUBIK_TELEMETRY_MAP_LEN different
					 * attributes in a telemetry map.
					 */
					if (tc_len == sizeof(hqubik.settings.telemetry_map[0]) *
					    QUBIK_TELEMETRY_MAP_LEN)
						for (int x = 0, y = 0; x < QUBIK_TELEMETRY_MAP_LEN;
						     x++, y += 2)
							hqubik.settings.telemetry_map[x] =
							        (tc_buffer[y] << 8) | tc_buffer[y + 1];

					/*
					 * Do we have to write if we didn't calculate the map,
					 * i.e. if tc_len above was wrong?
					 */
					qubik_write_settings(&hqubik);
					break;
				case 2:		/* Request periodic telemetry attributes */
					retrieve_meta(&hqubik, &meta);
					for (int i = 0; i < sizeof(hqubik.settings.telemetry_map); i += 2) {
						tm_attr_req_buf[i] = (hqubik.settings.telemetry_map[i / 2] >> 8) & 0xff;
						tm_attr_req_buf[i + 1] = hqubik.settings.telemetry_map[i / 2] & 0xff;
					}
					transmit_tm(tm_attr_req_buf, sizeof(hqubik.settings.telemetry_map),
					            VCID_MANAGEMENT, &meta);
					break;
				case 3: /* Kill switch */
					/* Compute the SHA-256 of the received key */
					sha256_init(&sha_ctx);
					sha256_update(&sha_ctx, tc_buffer, 24);
					sha256_final(&sha_ctx, sha_res);

					/* Check if the received key matches the stored hash */
					for (int i = 0; i < 24; i++) {
						if (sha_res[i] != hqubik.settings.sha256[i]) {
							ret = 1;
							break;
						}
					}
					if (ret == 1) {
						ret = 0;
						break;
					}
					if (hqubik.settings.mute_flag == 1) {
						hqubik.settings.mute_flag = 0;
					} else if (hqubik.settings.mute_flag == 0) {
						hqubik.settings.mute_flag = 1;
					}
					break;
				case 4:		/* Deploy antenna */
					/* Compute the SHA-256 of the received key */
					sha256_init(&sha_ctx);
					sha256_update(&sha_ctx, tc_buffer, 24);
					sha256_final(&sha_ctx, sha_res);

					/* Check if the received key matches the stored hash */
					for (int i = 0; i < 24; i++) {
						if (sha_res[i] != hqubik.settings.sha256[i]) {
							ret = 1;
							break;
						}
					}
					if (ret == 1) {
						ret = 0;
						break;
					}
					hqubik.settings.antenna_status.ant_deploy_status = antenna_deploy(&hwdg, wdgid);
					hqubik.settings.holdoff_timer = QUBIK_POST_DEPLOY_HOLDOFF_TIME;
					hqubik.settings.first_deploy = 0;
					hqubik.settings.antenna_first_deploy = 0;
					qubik_write_settings(&hqubik);
					break;
				/* Set the turn-around */
				case 5: {
					uint16_t delay_ms = (tc_buffer[0] << 8) | tc_buffer[1];
					if (delay_ms >= QUBIK_MIN_TRX_TURNAROUND_MS
					    && delay_ms <= QUBIK_MAX_TRX_TURNAROUND_MS) {
						hqubik.settings.trx_delay_ms = delay_ms;
					}
					qubik_write_settings(&hqubik);
				}
				break;
				case 6: /* toggle digipeater mode */
#if 0 /* FIXME: decide if SHA256 protection */
					/* Compute the SHA-256 of the received key */
					sha256_init(&sha_ctx);
					sha256_update(&sha_ctx, tc_buffer, 24);
					sha256_final(&sha_ctx, sha_res);

					/* Check if the received key matches the stored hash */
					for (int i = 0; i < 24; i++) {
						if (sha_res[i] != hqubik.settings.sha256[i]) {
							ret = 1;
							break;
						}
					}
					if (ret == 1) {
						ret = 0;
						break;
					}
#endif
					if (hqubik.settings.digipeater_enabled == 1) {
						hqubik.settings.digipeater_enabled = 0;
						printf("-> Disable digipeater!\n");
						retrieve_meta(&hqubik, &meta);
						transmit_tm("AMSAT-DL ERMINAZ-1U DIGIPEATER DISABLED!", 40,
						            VCID_DIGIPEATER, &meta);
					} else if (hqubik.settings.digipeater_enabled == 0) {
						hqubik.settings.digipeater_enabled = 1;
						printf("-> Enable digipeater!\n");
						retrieve_meta(&hqubik, &meta);
						transmit_tm("AMSAT-DL ERMINAZ-1U DIGIPEATER ENABLED!", 39,
						            VCID_DIGIPEATER, &meta);
					}
					qubik_write_settings(&hqubik);
					break;
				case 7:
					/* Parse the digipeater parameters */
					uint8_t message[OSDLP_MAX_TC_PAYLOAD_SIZE];

					if (!hqubik.settings.digipeater_enabled) {
						printf("ERROR: trying to access disabled repeater!\n");
						continue;
					}
					memcpy(message, tc_buffer + 6, tc_len - 5);
					message[tc_len - 5] = 0;
					printf("%d:%d> repeating message '%s', len=%d\n", VCID_MANAGEMENT, map, message,
					       tc_len);

					retrieve_meta(&hqubik, &meta);
					transmit_tm(tc_buffer, tc_len, VCID_DIGIPEATER, &meta);
					break;
			}
		}
	}
	/* USER CODE END start_mgmt_task */
}

/* USER CODE BEGIN Header_start_tm_rqst_task */
/**
 * @brief Function implementing the tm_request_task thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_start_tm_rqst_task */
void
start_tm_rqst_task(void const *argument)
{
	/* USER CODE BEGIN start_tm_rqst_task */
	uint16_t tm_length;
	uint8_t map;
	struct tx_frame_metadata meta;
	uint8_t wdgid;
	int ret = watchdog_register(&hwdg, &wdgid, "tm_rqst");

	if (ret != NO_ERROR) {
		Error_Handler();
	}

	/* Infinite loop */
	for (;;) {
		watchdog_reset_subsystem(&hwdg, wdgid);
		osDelay(10);
		if (!receive_tc(tm_request_buf, &tm_length, &map, VCID_REQ_TM)) {
			switch (map) {
				case 1:
					retrieve_meta(&hqubik, &meta);
					tm_length = telemetry_basic_frame(&hqubik, tm_request_buf);
					transmit_tm(tm_request_buf, tm_length, VCID_REQ_TM, &meta);
					break;
				case 2:
					retrieve_meta(&hqubik, &meta);
					transmit_tm((uint8_t *) manifesto, sizeof(manifesto),
					            VCID_REQ_TM, &meta);
					break;
				case 3:
					retrieve_meta(&hqubik, &meta);
					tm_length = telemetry_imu_frame(&hqubik, tm_request_buf);
					transmit_tm(tm_request_buf, tm_length, VCID_REQ_TM, &meta);
					break;
			}
		}
	}
	/* USER CODE END start_tm_rqst_task */
}

/* USER CODE BEGIN Header_start_digi_task */
/**
 * @brief Function implementing the digipeater_task thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_start_digi_task */
void
start_digi_task(void const *argument)
{
	/* USER CODE BEGIN start_digi_task */
	digipeater_task();
	/* USER CODE END start_digi_task */
}

/* telemetry_timer_callback function */
void
telemetry_timer_callback(void const *argument)
{
	/* USER CODE BEGIN telemetry_timer_callback */
	telemetry_tx_callback(&hqubik);
	/* USER CODE END telemetry_timer_callback */
}

/* incr_uptime function */
void
incr_uptime(void const *argument)
{
	/* USER CODE BEGIN incr_uptime */
	hqubik.uptime_secs++;
	hwdg.last_contact_secs++;
	/* USER CODE END incr_uptime */
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void
HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	/* USER CODE BEGIN Callback 0 */

	/* USER CODE END Callback 0 */
	if (htim->Instance == TIM1) {
		HAL_IncTick();
	}
	/* USER CODE BEGIN Callback 1 */

	/* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void
Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	while (1) {
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void
assert_failed(uint8_t *file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
	   tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
