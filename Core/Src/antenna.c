/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "antenna.h"
#include "watchdog.h"
#include "qubik.h"
#include <stdlib.h>

extern struct qubik hqubik;

/**
 * @brief Antenna deployment power control
 * Controls power (On/Off) to the antenna release mechanism
 * @param state ant_deploy_power_t value
 */
void
antenna_deploy_power(ant_deploy_power_t state)
{
	HAL_GPIO_WritePin(ANT_Deploy_GPIO_Port, ANT_Deploy_Pin, state);
}

/**
 * @brief Antenna deployment status
 * Provides information whether the antenna is deployed or stowed.
 * Performs multiple reading to ensure the correct reading is provided
 * @return ant_deploy_status_t value
 */
ant_deploy_status_t
antenna_deploy_status()
{
	uint8_t ret = 0;
	uint8_t on = 0;
	uint8_t off = 0;
	for (uint8_t cnt = 0; cnt < ANT_STATUS_DEBOUNCE_CNT; cnt++) {
		ret = HAL_GPIO_ReadPin(ANT_Sense_GPIO_Port, ANT_Sense_Pin);
		if (ret) {
			on++;
		} else {
			off++;
		}
		osDelay(10);
	}
	if (on > ANT_STATUS_DEBOUNCE_CNT * 3 / 4) {
		return ANT_DEPLOYED;
	} else if (off > ANT_STATUS_DEBOUNCE_CNT * 3 / 4) {
		return ANT_STOWED;
	} else {
		return ANT_STOWED;
	}
}

/**
 * @brief Tests antenna deploy mechanism
 *
 * Performs a deploy mechanisms test by applying power to the deploy mechanism for a short
 * period and measuring current drawn an voltage level.
 *
 * This will provide information regarding any problems in the control mechanism as well as
 * information about battery capability for full deployment.
 *
 * Voltage and current values are stored in power_status structure
 *
 *
 * @param power_status power status structure
 * @return Test result
 */
ant_deploy_test_status_t
antenna_deploy_test()
{
	int16_t start_current;

	start_current = max17261_get_current(&hqubik.hmax17261);

	antenna_deploy_power(ANT_DEPLOY_ON);
	osDelay(ANT_DEPLOY_TEST_TIME);
	hqubik.settings.antenna_pwr_stats.ant_deploy_test_current =
	        max17261_get_current(
	                &hqubik.hmax17261) - start_current;
	hqubik.settings.antenna_pwr_stats.ant_deploy_test_voltage =
	        max17261_get_voltage(&hqubik.hmax17261);
	antenna_deploy_power(ANT_DEPLOY_OFF);

	if (abs(hqubik.settings.antenna_pwr_stats.ant_deploy_test_current) <
	    (ANT_DEPLOY_TEST_CURRENT /
	     3)) {
		return ANT_DEPLOY_TEST_FAIL;
	}
	if (abs(hqubik.settings.antenna_pwr_stats.ant_deploy_test_current) <
	    ANT_DEPLOY_TEST_CURRENT) {
		return ANT_DEPLOY_TEST_DEGRADED;
	}
	return ANT_DEPLOY_TEST_OK;
}

/**
 * @brief Antenna deploy sequence activation
 * Starts the antenna deploy activation sequence. Current is applied on the deploy resistors until
 * either antenna deploy status is changed or timeout occurs
 *
 * @param hwdg the watchdog structure
 * @param wdgid the watchdog id
 * @return antenna deployed status
 */
uint8_t
antenna_deploy(struct watchdog *hwdg, uint8_t wdgid)
{
	uint16_t cntr = 0;
	uint32_t start_uptime = 0;

	ant_deploy_status_t status = antenna_deploy_status();

	antenna_deploy_power(ANT_DEPLOY_ON);
	start_uptime = hqubik.uptime_secs;
	switch (status) {
		case ANT_DEPLOYED:	// Faulty switch, burn max time
			hqubik.settings.antenna_status.antenna_retry_count++;
			qubik_write_settings(&hqubik);
			for (cntr = 0; cntr < ANT_DEPLOY_TIME; cntr++) {
				watchdog_reset_subsystem(hwdg, wdgid);
				osDelay(1000);
			}
			antenna_deploy_power(ANT_DEPLOY_OFF);
			status = ANT_ASSUMED_DEPLOYED;
			break;
		case ANT_STOWED:
			while ((status == ANT_STOWED) && (cntr++ < (ANT_DEPLOY_TIME * 10))) {
				osDelay(100);
				status = antenna_deploy_status();
				watchdog_reset_subsystem(hwdg, wdgid);
			}
			osDelay(ANTENNA_POST_DEPLOY_DELAY);
			antenna_deploy_power(ANT_DEPLOY_OFF);
			watchdog_reset_subsystem(hwdg, wdgid);
			status = antenna_deploy_status();
			break;
		default:
			break;
	}
	hqubik.settings.antenna_status.antenna_deploy_time = hqubik.uptime_secs -
	        start_uptime;
	return status;
}
