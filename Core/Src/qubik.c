/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>

#include "qubik.h"
#include "error.h"
#include "conf.h"
#include "telemetry.h"
#include "cmsis_os.h"
#include "utils.h"

extern uint8_t reset_flag;
typedef enum {
	RESET_CAUSE_UNKNOWN = 0,
	RESET_CAUSE_LOW_POWER_RESET,
	RESET_CAUSE_WINDOW_WATCHDOG_RESET,
	RESET_CAUSE_INDEPENDENT_WATCHDOG_RESET,
	RESET_CAUSE_SOFTWARE_RESET,
	RESET_CAUSE_POWER_ON_POWER_DOWN_RESET,
	RESET_CAUSE_EXTERNAL_RESET_PIN_RESET,
	RESET_CAUSE_BROWNOUT_RESET,
} reset_cause_t;

static uint16_t
encode_telemetry_entry(radio_baud_t b, radio_encoding_t enc,
                       radio_modulation_t mod, telemetry_t t, int8_t txpwr,
                       uint8_t enable_pa)
{
	uint16_t r = b | (enc << 3) | (mod << 5) | (t << 8)
	             | (telemetry_tx_power_index(txpwr) << 11)
	             | (enable_pa << 15);
	return r;
}


void
qubik_update_ror_counters(struct qubik *q);

static void
default_settings(struct qubik_settings *s)
{
	compile_assert_positive(224 - sizeof(struct qubik_settings));

	/*
	 * To make sure that the settings are reset if a firmware is flashed
	 * on a different satellite that have been previously flashed with
	 * a different SCID, we compare with both the flash magic value and
	 * the SCID
	 */
	s->init = QUBIK_FLASH_MAGIC_VAL + OSDLP_SCID;
	s->scid = OSDLP_SCID;
	s->first_deploy = 1;
	s->sync_word = QUBIK_SYNC_WORD;
	s->rx_freq = QUBIK_RX_FREQ_HZ;
	s->tx_freq = QUBIK_TX_FREQ_HZ;
	s->antenna_first_deploy = 1;
	s->holdoff_timer = 0;

	/* At least make sure that everything contains valid data */
	for (uint32_t i = 0; i < QUBIK_TELEMETRY_MAP_LEN; i++) {
		s->telemetry_map[i] = encode_telemetry_entry(
		                              RADIO_BAUD_9600, RADIO_ENC_RS,
		                              RADIO_MOD_FSK, TELEMETRY_BASIC,
		                              QUBIK_DEFAULT_TX_POWER, 1);
	}

	/* Apply specific mappings */
	s->telemetry_map[0] = encode_telemetry_entry(RADIO_BAUD_600,
	                      RADIO_ENC_RS,
	                      RADIO_MOD_BPSK_RES,
	                      TELEMETRY_BASIC,
	                      QUBIK_DEFAULT_TX_POWER,
	                      1);

	s->telemetry_map[1] = encode_telemetry_entry(RADIO_BAUD_600,
	                      RADIO_ENC_RS,
	                      RADIO_MOD_BPSK,
	                      TELEMETRY_IMU,
	                      QUBIK_DEFAULT_TX_POWER,
	                      1);

	s->telemetry_map[2] = encode_telemetry_entry(RADIO_BAUD_38400,
	                      RADIO_ENC_RS,
	                      RADIO_MOD_BPSK_RES,
	                      TELEMETRY_BASIC,
	                      QUBIK_DEFAULT_TX_POWER,
	                      1);

	s->telemetry_map[3] = encode_telemetry_entry(RADIO_BAUD_38400,
	                      RADIO_ENC_RS,
	                      RADIO_MOD_BPSK,
	                      TELEMETRY_IMU,
	                      QUBIK_DEFAULT_TX_POWER,
	                      1);

	s->telemetry_map[4] = encode_telemetry_entry(RADIO_BAUD_600,
	                      RADIO_ENC_RS,
	                      RADIO_MOD_FSK,
	                      TELEMETRY_BASIC,
	                      QUBIK_DEFAULT_TX_POWER,
	                      1);

	s->telemetry_map[5] = encode_telemetry_entry(RADIO_BAUD_600,
	                      RADIO_ENC_RS,
	                      RADIO_MOD_FSK,
	                      TELEMETRY_IMU,
	                      QUBIK_DEFAULT_TX_POWER,
	                      1);

	s->telemetry_map[6] = encode_telemetry_entry(RADIO_BAUD_1200,
	                      RADIO_ENC_RS,
	                      RADIO_MOD_FSK,
	                      TELEMETRY_BASIC,
	                      QUBIK_DEFAULT_TX_POWER,
	                      1);
	s->telemetry_map[7] = encode_telemetry_entry(RADIO_BAUD_1200,
	                      RADIO_ENC_RS,
	                      RADIO_MOD_FSK,
	                      TELEMETRY_IMU,
	                      QUBIK_DEFAULT_TX_POWER,
	                      1);

	s->telemetry_map[8] = encode_telemetry_entry(RADIO_BAUD_9600,
	                      RADIO_ENC_RS,
	                      RADIO_MOD_FSK,
	                      TELEMETRY_BASIC,
	                      QUBIK_DEFAULT_TX_POWER,
	                      1);

	s->telemetry_map[9] = encode_telemetry_entry(RADIO_BAUD_9600,
	                      RADIO_ENC_RS,
	                      RADIO_MOD_FSK,
	                      TELEMETRY_IMU,
	                      QUBIK_DEFAULT_TX_POWER,
	                      1);

	s->telemetry_map[10] = encode_telemetry_entry(RADIO_BAUD_9600,
	                       RADIO_ENC_RS,
	                       RADIO_MOD_FSK,
	                       TELEMETRY_BASIC,
	                       15,
	                       1);

	s->telemetry_map[11] = encode_telemetry_entry(RADIO_BAUD_9600,
	                       RADIO_ENC_RS,
	                       RADIO_MOD_FSK,
	                       TELEMETRY_IMU,
	                       15,
	                       1);

	s->tm_resp_tx_power = QUBIK_DEFAULT_TX_POWER;
	s->antenna_status.ant_deploy_status = ANT_STOWED;
	s->antenna_status.ant_deploy_test = ANT_DEPLOY_TEST_PENDING;
	s->antenna_status.antenna_deploy_time = 0;
	s->antenna_status.antenna_retry_count = 0;
	s->tm_resp_enc = RADIO_ENC_RS;
	s->tm_resp_mod = RADIO_MOD_FSK;
	s->tm_resp_baud = RADIO_BAUD_9600;
	s->tm_resp_pa_en = 1;
	s->antenna_pwr_stats.ant_deploy_test_voltage = 0;
	s->antenna_pwr_stats.ant_deploy_test_current = 0;
	s->antenna_pwr_stats.ant_deploy_avg_voltage = 0;
	s->antenna_pwr_stats.ant_deploy_avg_current = 0;
	s->reset_counters.brownout_counter = 0;
	s->reset_counters.independent_watchdog_counter = 0;
	s->reset_counters.low_power_counter = 0;
	s->reset_counters.software_counter = 0;
	s->mute_flag = 0;
	const uint8_t sha[24] = {0x11, 0x18, 0x12, 0xff, 0x8a, 0xc7, 0xea, 0xb7, 0x3c, 0x21,
	                         0xcc, 0x12, 0x65, 0xdf, 0x4a, 0x97, 0xef, 0xfb, 0x7b, 0x3f, 0x78, 0x6e, 0x55, 0x56
	                        }; /* FIXME: update for ERMINAZ-1 mission! */
	memcpy(s->sha256, sha, 24 * sizeof(uint8_t));
	s->trx_delay_ms = QUBIK_DEFAULT_TRX_TURNAROUND_MS;
	s->digipeater_enabled = 0;  /* Disable digipeater by default! */
}

static int
qubik_read_settings(struct qubik *q)
{
	if (!q) {
		return -INVAL_PARAM;
	}
	int ret = ft_storage_read(&q->ft_persist_mem, &q->settings);
	/* In case the read failed, restore the defaults and write back the memory*/
	if (ret) {
		default_settings(&q->settings);
		return qubik_write_settings(q);
	}
	/* Maybe the flash was not initialized.
	 * For the usage of both QUBIK_FLASH_MAGIC_VAL and OSDLP_SCID please
	 * check the reasoning provided at default_settings()
	 */
	if (q->settings.init != QUBIK_FLASH_MAGIC_VAL + OSDLP_SCID) {
		default_settings(&q->settings);
		return qubik_write_settings(q);
	}
	return NO_ERROR;
}

int
qubik_init(struct qubik *q)
{
	if (!q) {
		return -INVAL_PARAM;
	}
	memset(q, 0, sizeof(struct qubik));

	/* Wait power to stabilize before touching the flash */
	HAL_Delay(1000);

	/* Initialize the fault tolerant storage engine */
	int ret = ft_storage_init(&q->ft_persist_mem,
	                          sizeof(struct qubik_settings),
	                          QUBIK_STORAGE_FLASH_ADDR);
	if (ret) {
		return ret;
	}
	ret = qubik_read_settings(q);
	if (ret) {
		return ret;
	}

	// Update ROR counter fields
	q->reason_of_reset = reset_flag;
	qubik_update_ror_counters(q);
	// Write back to storage
	qubik_write_settings(q);

#ifndef QEMU
	/* Initialize radio related structures */
	ret = radio_init(&q->hradio);
	if (ret) {
		return ret;
	}
#endif

	/* Setup MAX17261 */
	q->hmax17261.DesignCap = BATTERY_CAPACITY;
	q->hmax17261.IchgTerm = BATTERY_CRG_TERM_I;
	q->hmax17261.VEmpty = (BATTERY_V_EMPTY << 7) | (BATTERY_V_Recovery & 0x7F);
	q->hmax17261.R100 = 1;
	q->hmax17261.ChargeVoltage = POWER_CHG_VOLTAGE;
	q->hmax17261.lparams.FullCapNom = BATTERY_CAPACITY;
	q->hmax17261.lparams.FullCapRep = BATTERY_CAPACITY;
	q->hmax17261.lparams.RCOMP0 = MAX17261_RCOMP0_VAL;
	q->hmax17261.lparams.TempCo = MAX17261_TEMP_CO_VAL;
	q->hmax17261.lparams.QRTable[0] = MAX17261_QRTABLE01_VAL;
	q->hmax17261.lparams.QRTable[1] = MAX17261_QRTABLE02_VAL;
	q->hmax17261.lparams.QRTable[2] = MAX17261_QRTABLE03_VAL;
	q->hmax17261.lparams.QRTable[3] = MAX17261_QRTABLE04_VAL;
	q->hmax17261.lparams.cycles = 0;
	q->hmax17261.init_option = MAX17261_INIT_OPTION;
	q->hmax17261.force_init = q->settings.first_deploy;

#ifndef QEMU
	max17261_init(&q->hmax17261);
#endif

	q->hmax17261.force_init = 0;
	if (q->settings.first_deploy) {
		max17261_reset_minmax_voltage(&q->hmax17261);
		max17261_reset_minmax_current(&q->hmax17261);
		max17261_reset_minmax_temperature(&q->hmax17261);
	}

	/* Initial FSM state */
	q->fsm_state = FSM_POST_DEPLOY_HOLDOFF;

	return NO_ERROR;
}

int
qubik_write_settings(struct qubik *q)
{
	if (!q) {
		return -INVAL_PARAM;
	}

#ifndef QEMU
	return ft_storage_write(&q->ft_persist_mem, &q->settings);
#else
	return NO_ERROR;
#endif
}

void
qubik_update_ror_counters(struct qubik *q)
{
	if (q->reason_of_reset & (1UL << (RESET_CAUSE_LOW_POWER_RESET))) {
		q->settings.reset_counters.low_power_counter++;
	}
	if (q->reason_of_reset & (1UL << (RESET_CAUSE_INDEPENDENT_WATCHDOG_RESET))) {
		q->settings.reset_counters.independent_watchdog_counter++;
	}
	if (q->reason_of_reset & (1UL << (RESET_CAUSE_SOFTWARE_RESET))) {
		q->settings.reset_counters.software_counter++;
	}
	if (q->reason_of_reset & (1UL << (RESET_CAUSE_BROWNOUT_RESET))) {
		q->settings.reset_counters.brownout_counter++;
	}
}
