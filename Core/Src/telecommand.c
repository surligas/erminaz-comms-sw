/*
 *
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "telecommand.h"
#include <string.h>

extern osMutexId osdlp_rx_mtxHandle;

int
receive_tc(uint8_t *pkt, uint16_t *length, uint8_t *cmd_id, uint8_t vcid)
{
	struct rx_bundle rx_bun;
	if (pkt == NULL || length == NULL || cmd_id == NULL) {
		return -OSDLP_NULL;
	}
	int ret = 0;
	if (osMutexWait(osdlp_rx_mtxHandle, 200) == osOK) {
		if (rx_queues[vcid].inqueue > 0) {
			ret = dequeue(&rx_queues[vcid], &rx_bun, length);
			*length = rx_bun.frame_len;
			*cmd_id = rx_bun.mapid;
			if (*length > OSDLP_MAX_TC_FRAME_SIZE) {
				osMutexRelease(osdlp_rx_mtxHandle);
				printf("ERROR: TC frame size exceeded!\n");
				return -OSDLP_QUEUE_MEM_ERROR;
			}
			printf("dequeued %d:%d\n", vcid, *cmd_id);
			memcpy(pkt, rx_bun.frame, *length * sizeof(uint8_t));
		} else {
			ret = -OSDLP_QUEUE_EMPTY;
		}
		osMutexRelease(osdlp_rx_mtxHandle);
	} else {
		return -OSDLP_MTX_LOCK;
	}
	return ret;
}
