/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "queue_util.h"
#include <stdlib.h>
#include <string.h>
#include <Drivers/OSDLP/include/osdlp_tc.h>
#include <Drivers/OSDLP/include/osdlp_cop.h>
#include "error.h"

int
init_queue(struct queue *handle,
           uint16_t item_size,
           uint16_t num_items,
           uint8_t *mem_space)
{
	handle->capacity = num_items;
	handle->head = 0;
	handle->inqueue = 0;
	handle->tail = 0;
	handle->item_size = item_size;
	handle->mem_space = mem_space;
	if (handle->mem_space == NULL) {
		handle->is_init = OSDLP_QUEUE_MEM_ERROR;
		return -OSDLP_QUEUE_MEM_ERROR;
	}
	handle->is_init = NO_ERROR;
	return NO_ERROR;
}

int
enqueue(struct queue *que, void *pkt, uint16_t len)
{
	if (que->inqueue < que->capacity) {
		/* Copy length info at the start of the buffer */
		memcpy(&que->mem_space[(que->tail * que->item_size)], &len, sizeof(uint16_t));
		memcpy(&que->mem_space[(que->tail * que->item_size) + sizeof(uint16_t)],
		       (uint8_t *)pkt,
		       len * sizeof(uint8_t));
		que->tail++;
		if (que->tail >= que->capacity) {
			que->tail = 0;
		}
		que->inqueue++;
		return NO_ERROR;
	} else {
		return -OSDLP_QUEUE_FULL;
	}
}

int
enqueue_now(struct queue *que, void *pkt, uint16_t len)
{
	if (que->inqueue < que->capacity) {
		/* Copy length info at the start of the buffer */
		memcpy(&que->mem_space[(que->tail * que->item_size)], &len, sizeof(uint16_t));
		memcpy(&que->mem_space[(que->tail * que->item_size) + sizeof(uint16_t)],
		       (uint8_t *)pkt,
		       len * sizeof(uint8_t));
		que->tail++;
		if (que->tail >= que->capacity) {
			que->tail = 0;
		}
		que->inqueue++;
		return NO_ERROR;
	} else { // Overwrite back
		memcpy(&que->mem_space[(que->tail * que->item_size)], &len, sizeof(uint16_t));
		memcpy(&que->mem_space[(que->tail * que->item_size) + sizeof(uint16_t)],
		       (uint8_t *)pkt,
		       len * sizeof(uint8_t));
		return NO_ERROR;
	}
}

int
dequeue(struct queue *que, void *pkt, uint16_t *len)
{
	if (que->inqueue > 0) {
		memcpy(len, &que->mem_space[(que->head * que->item_size)], sizeof(uint16_t));
		memcpy((uint8_t *)pkt, &que->mem_space[(que->head * que->item_size) + sizeof(
		                uint16_t)],
		       *len * sizeof(uint8_t));
		que->head++;
		if (que->head >= que->capacity) {
			que->head = 0;
		}
		que->inqueue--;
		return NO_ERROR;
	} else {
		return -OSDLP_QUEUE_EMPTY;
	}
}

int
reset_queue(struct queue *que)
{
	que->tail = 0;
	que->head = 0;
	que->inqueue = 0;
	return NO_ERROR;
}

bool
full(struct queue *que)
{
	if (que->capacity - que->inqueue > 0) {
		return false;
	} else {
		return true;
	}
}

int
front(struct queue *que, uint8_t *item)
{
	if (que->tail == que->head) {
		return -OSDLP_QUEUE_EMPTY;
	}
	item = &que->mem_space[(que->head * que->item_size) + sizeof(uint16_t)];
	return NO_ERROR;
}

int
back(struct queue *que, uint8_t *item)
{
	if (que->tail == que->head) {
		return -OSDLP_QUEUE_EMPTY;
	}
	if (que->tail > 0) {
		item = &que->mem_space[((que->tail - 1) * que->item_size) + sizeof(uint16_t)];
		return NO_ERROR;
	} else {
		item = &que->mem_space[((que->capacity - 1) * que->item_size) + sizeof(
		                               uint16_t)];
		return NO_ERROR;
	}
}

int
get_item(struct queue *que, uint8_t *item, uint16_t pos)
{
	if (pos >= que->capacity || pos >= que->inqueue) {
		return -OSDLP_QUEUE_EMPTY;
	}
	if (que->head + pos < que->capacity) {
		item = &que->mem_space[((que->head + pos) * que->item_size) + sizeof(uint16_t)];
		return NO_ERROR;
	} else {
		item = &que->mem_space[((que->head + pos - que->capacity) * que->item_size) +
		                       sizeof(uint16_t)];
		return NO_ERROR;
	}
}

